<?php
  // Adapted for The Art of Web: www.the-art-of-web.com
  // Please acknowledge use of this code by including this header.

  // initialise image with dimensions of 160 x 45 pixels
  $image = @imagecreatetruecolor(160, 45) or die("Cannot Initialize new GD image stream");

  // set background and allocate drawing colours
  $bgroundarray = [];
  $bgroundarray[] = imagecolorallocate($image, 0x66, 0xCC, 0xFF);//lightblue
  $bgroundarray[] = imagecolorallocate($image, 34, 145, 3);//green
  $bgroundarray[] = imagecolorallocate($image, 110, 35, 3);//darkorange
  $bgroundarray[] = imagecolorallocate($image, 151, 39, 168);//purple
  imagefill($image, 0, 0, $bgroundarray[array_rand($bgroundarray)]);

  // set line color
  $linecolorarray = [];
  $linecolorarray[] = imagecolorallocate($image, 0x33, 0x99, 0xCC);//lightblue
  $linecolorarray[] = imagecolorallocate($image, 130, 247, 67);//lightgreen
  $linecolorarray[] = imagecolorallocate($image, 250, 27, 27);//red

  // set text colors
  $txtcolorarray = [];
  $txtcolorarray[] = imagecolorallocate($image, 0x00, 0x00, 0x00);//black
  $txtcolorarray[] = imagecolorallocate($image, 0xFF, 0xFF, 0xFF);//white
  //$txtcolorarray[] = imagecolorallocate($image, 245, 66, 224);//pink
  //$txtcolorarray[] = imagecolorallocate($image, 21, 3, 156);//darkblue

  // draw random lines on canvas
  for($i=0; $i < 8; $i++) {
    imagesetthickness($image, rand(1,3));
    imageline($image, rand(0,160), 0, rand(0,160), 45, $linecolorarray[array_rand($linecolorarray)]);
  }

  session_start();

  // using a mixture of TTF fonts
  $fonts = [];
  $fonts[] = "ttf/DejavuSansMono.ttf";
  $fonts[] = "ttf/DejavuSansMonoBold.ttf";
  $fonts[] = "ttf/Fancytext.ttf";
  $fonts[] = "ttf/GrungeHandwriting.ttf";

  // add random digits to canvas using random colour
  $digit = '';
  for($x = 10; $x <= 130; $x += 30) {
    //$textcolor = (rand() % 2) ? $textcolor1 : $textcolor2;
    $textcolor = $txtcolorarray[array_rand($txtcolorarray)];
    $digit .= ($num = rand(0, 9));
    imagettftext($image, 20, rand(-30,30), $x, rand(20, 42), $textcolor, $fonts[array_rand($fonts)], $num);
  }

  // record digits in session variable
  $_SESSION['digit'] = $digit;

  // display image and clean up
  header('Content-type: image/png');
  imagepng($image);
  imagedestroy($image);
?>