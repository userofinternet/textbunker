<?php
// THIS SOFTWARE IS RELEASED UNDER THE MIT LICENSE.
// CREATED BY TEXTBUNKER.NET

// Print html
function printDoc($message) {
	echo "<!DOCTYPE html>";
	echo "<html lang='en'>";
	echo "<head>";
	echo "<title>";
	echo "textBUNKER";
	echo "</title>";
	echo "<meta charset='UTF-8'>";
	echo "<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />";
	echo "<link rel='stylesheet' type='text/css' href='forest.css'>";
	echo "</head>";
	echo "<body>";
	echo "<div id='cnt'>";
	echo "<div id='logoCnt'><img class='logoBanner' src='logo.png' alt='text BUNKER'></div>";
    echo "<div class='navBar'>";
    echo "<a href='/'>home</a>";
    echo "<a href='/siteinfo.php?i=about'>about</a>";
    echo "<a href='/index.php?b=paranormal'>paranormal</a>";
    echo "<a href='/index.php?b=tech'>tech</a>";
	echo "<a href='/index.php?b=science'>science</a>";
	echo "<a href='/index.php?b=general'>general</a>";
	echo "<a href='/index.php?b=politics'>politics</a>";
    echo "<a href='/index.php?b=news'>news</a>";
    echo "<a href='/index.php?b=shtf'>shtf</a>";
	echo "<a href='/index.php?b=movies'>movies</a>";
	echo "<a href='/index.php?b=music'>music</a>";
    echo "<a href='/siteinfo.php?i=chat'>chat</a>";
	echo "<a href='/siteinfo.php?i=rss'>rss</a>";
    echo "</div>";
	echo "<div id='infoCnt'>";
	echo $message;
	echo "</div></div><body></html>";
}

function printHeader($_dbName, $_title) {
    echo "<meta charset='UTF-8'>";
    echo "<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />";
    echo "<meta name='description' content='&quot;/$_dbName/&quot; is textbunkers board for discussion of $_title topics.'>";
    echo "<meta name='keywords' content='survival,forum,shtf,paranormal,ufos,aliens,uap,textboard'>";
    echo "<link id='themeLink' rel='stylesheet' type='text/css' href='/forest.css'>";
	echo "<script src='/site.js'></script>";
    echo "<head><body onload='doWork()'><div id='cnt'>";
    echo "<div id='logoCnt'><img class='logoBanner' src='/logo.png' alt='text BUNKER'></div>";
    echo "<div class='navBar'>";
    echo "<a href='/'>home</a>";
    echo "<a href='/siteinfo.php?i=about'>about</a>";
	echo "<a href='/index.php?b=paranormal'>paranormal</a>";
	echo "<a href='/index.php?b=tech'>tech</a>";
	echo "<a href='/index.php?b=science'>science</a>";
	echo "<a href='/index.php?b=general'>general</a>";
	echo "<a href='/index.php?b=politics'>politics</a>";
	echo "<a href='/index.php?b=news'>news</a>";
	echo "<a href='/index.php?b=shtf'>shtf</a>";
	echo "<a href='/index.php?b=movies'>movies</a>";
	echo "<a href='/index.php?b=music'>music</a>";
	echo "<a href='/siteinfo.php?i=chat'>chat</a>";
	echo "<a href='/siteinfo.php?i=rss'>rss</a>";
	echo "<div class='leftNavItems'>";
	echo "<a id='theme' onclick='changeTheme()'>💡</a>";
	echo "</div>";
    echo "</div>";
	$current_date = date('m-d-Y H:i:s');
	$timezone = date_default_timezone_get();
	echo "<div class='updown' onclick='scrollToElement(1)'>🔽</div>";
	echo "<div id='serverTime'>";
	echo "<div id='currentDate'>Server time: ".$current_date." ".$timezone."</div>";
	echo "</div><br>";
    echo "<div class='catagory'><a class='catagoryLink' href='index.php?b=$_dbName'><< /$_dbName/</a>";
	echo "<div class='newThreadButton'><a onclick='moveReply(9999)'>[new reply]</a></div>";
	echo "</div><br>";
}

// Cross Site Script  & Code Injection Sanitization
function xss_cleaner($input_str) {
    $return_str = str_replace( array('<',';','|','&','>',"'",'"',')','('), array('&lt;','&#58;','&#124;','&#38;','&gt;','&apos;','&#x22;','&#x29;','&#x28;'), $input_str );
    $return_str = str_ireplace( '%3Cscript', '', $return_str );
    return $return_str;
}

if ( empty($_GET['b']) ) {
	http_response_code(400);
	die("No board with that name exists on this server.");
}

// Get board URL variables
switch ($_GET['b']) {
	case "news":
		$dbname = "news";
		$title = "News";
		break;
	case "paranormal":
		$dbname = "paranormal";
		$title = "Paranormal";
		break;
	case "tech":
		$dbname = "tech";
		$title = "Technology";
		break;
	case "shtf":
		$dbname = "shtf";
		$title = "Survival and Preparedness";
		break;
	case "general":
		$dbname = "general";
		$title = "General off-topic discussions";
		break;
	case "science":
		$dbname = "science";
		$title = "Science";
		break;
	case "politics":
		$dbname = "politics";
		$title = "Politics";
		break;
	case "movies":
		$dbname = "movies";
		$title = "Movies & TV";
		break;
	case "music":
		$dbname = "music";
		$title = "Music";
		break;
	default:
        http_response_code(404);
		die("No board with that name exists on this server.");
}

if ( empty($_GET['t']) ) {
    http_response_code(400);
    die("No thread ID was given.");
}

//DB Credentials
$serverName = "localhost";
$userName = "textboarduser";
$password = "ndsfu734Y%#$";

$conn = mysqli_connect($serverName, $userName, $password, $dbname);
if ( mysqli_connect_errno() ){
	printDoc("<br>Connection to database failed.");
	die();
}

//load threadID via get request
//select thread by ID and loop through and display comments.
$threadID_dirty = xss_cleaner($_GET['t']);
$threadID = mysqli_real_escape_string($conn, $threadID_dirty);
$sql = "SELECT postDate, threadOP FROM $threadID";
$result = mysqli_query($conn, $sql);
// die if thread not found
if ($result === false) {
	mysqli_close($conn);
    http_response_code(404);
    printDoc("<br>404 Thread not found.");
	die();
}
$row = mysqli_fetch_assoc($result);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php
// Print the title of the thread
$len = mb_strlen($row['threadOP'], 'UTF-8');
if($len > 100){
	$title = substr($row['threadOP'], 0, 100);
	echo "<title>".htmlspecialchars_decode($title)."</title>";
} else{
	echo "<title>".htmlspecialchars_decode($row['threadOP'])."</title>";
}

printHeader($dbname, $title);
// Print thread OP
// Modify date to MDY
/*
$utcTime = substr($row['postDate'], 11);
$dateStr = substr($row['postDate'], 0, 10);
// Move year to the end of the date
$year = substr($dateStr, 0, 4);
$monthDay = substr($dateStr, 5);
$newDateStr = $monthDay . "-" . $year . " " . $utcTime;
*/
$newDateStr = $row['postDate'];
echo "<div class='comment'>";
echo "<div class='commentItems'>";
echo "<div class='time'>".$newDateStr." utc</div>";
echo "<a onclick='copyLinkToPost(1)' title='Click to quote.'>OP</a>";
echo "</div>";
echo "<div class='innerText'>".htmlspecialchars_decode($row['threadOP'])."</div>";
echo "<div class='commentBottom'><a onclick='moveReply(0)'>[reply]</a></div>";
echo "</div>";
// print empty div id 0
echo "<div id='0'></div>";
?>
<form id="replyForm" accept-charset="UTF-8">
	<div id='captchaWarning'></div>
    <label for="fname">Reply:</label><br>
	<?php
	if ( empty($_POST['data']) ) {
		echo "<textarea class='textContentBox' name='data' maxlength='3000' placeholder='Type your reply here. If you are replying to someone, include @number' required></textarea><br>";
	} else {
		echo "<textarea class='textContentBox' name='data' maxlength='3000' placeholder='Type your reply here. If you are replying to someone, include @number' value=".$_POST['data']. "required></textarea><br>";
	}
	?>
	<div class='mediaButtonCnt'>
	<div class='dropdown'>
	<button type='button' id='dropbtn'>Embed</button>
	<div id='dropdown-content'>
	<a onclick='insertQuoteFromReplyForm()'>Quote</a>
	<a onclick='insertimg()'>Image</a>
	<a onclick='insertYouTubeLink()'>YouTube</a>
	<a onclick='insertVideo()'>Video</a>
	<a onclick='insertLink()'>Link</a>
	</div></div></div>
	<div class='captchaCnt'>
	<button type='button' id='captchaBtn' onclick='showCaptcha()'>Get Captcha</button>
    <img class="captchaImage" src="" alt="CAPTCHA" loading="lazy">
	<a id='captchaRefreshButton' onclick='refreshCaptcha()'>🔄</a></div>
    <span>Captcha:</span><br>
    <input id="captchaInput" type="text" size="15" maxlength="5" name="captcha">
    <button type="button" class="submitButton" onclick="submitForm('<?php echo $dbname; ?>', '<?php echo $threadID_dirty; ?>')">Submit</button>
</form> 
<?php
$sql = "SELECT * FROM $threadID ORDER BY id ASC";
mysqli_free_result($result);
$result = mysqli_query($conn, $sql);

while($row = mysqli_fetch_assoc($result)){
	if (!empty($row['threadReply'])){
		/*
		// Modify date to MDY
		$utcTime = substr($row['postDate'], 11);
		$dateStr = substr($row['postDate'], 0, 10);
		// Move year to the end of the date
		$year = substr($dateStr, 0, 4);
		$monthDay = substr($dateStr, 5);
		$newDateStr = $monthDay . "-" . $year . " " . $utcTime;
		*/
		$newDateStr = $row['postDate'];
		echo "<div class='comment'>";
        echo "<div class='commentItems'>";
        echo "<div class='time'>".$newDateStr." utc</div>";
        echo "<a onclick='copyLinkToPost(".$row['id'].")' title='copy link to post.'>No.".$row['id']."</a>";
        echo "</div>";
        echo "<div class='innerText'>".htmlspecialchars_decode($row['threadReply'])."</div>";
		echo "<div class='commentBottom'><a onclick='moveReply(".$row['id'].")'>[reply]</a></div>";
		echo "</div>";
		// Print an empty div with the id set to the comment number
		echo "<div id=".$row['id']."></div>";
	}
}
//mysqli_free_result($result);
mysqli_close($conn);
?>
<?php echo "<div class='updown' onclick='scrollToElement(0)'>🔼</div>"; ?>
<div id="tid">
<?php echo "ID: ".$threadID_dirty; ?>
</div>
<?php
echo "<div id='bottom'>Copyright © ".date('Y')." textbunker.net all rights reserved.</div>";
echo "<div id='webRingContainer'>";
echo "<p>other cool sites:</p>";
// output php from webring.php
include 'webring.php';
echo "</div>";
?>
</div>
</body>
<script>loadFormEmbedControls()</script>
<script>displayLocalDate()</script>
</html>