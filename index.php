<?php
// THIS SOFTWARE IS RELEASED UNDER THE MIT LICENSE.
// CREATED BY TEXTBUNKER.NET CODEBERG.ORG/USEROFINTERNET

// Stop php from whining about GET variable not being set.
if ( !isset($_GET['b']) ) {
	$_GET['b'] = "index";
}

$serverName = "localhost";
$userName = "textboarduser";
$password = "ndsfu734Y%#$";

function printHeader($_dbName, $_title) {
	echo "<!DOCTYPE html>";
	echo "<html lang='en'>";
	echo "<head><title>";
	echo $_title;
	echo "</title>";
	echo "<meta charset='UTF-8'>";
	echo "<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />";
	echo "<meta name='description' content='&quot;/$_dbName/&quot; is textbunkers board for discussion of $_dbName topics.'>";
	echo "<meta name='keywords' content='survival,forum,shtf,paranormal,ufos,aliens,uap,textboard'>";
	echo "<link id='themeLink' rel='stylesheet' type='text/css' href='/forest.css'></head>";
	echo "<script src='/site.js'></script>";
	echo "<body onload='loadTheme()'>";
	echo "<div id='cnt'>";
	echo "<div id='logoCnt'><img class='logoBanner' src='/logo.png' alt='text BUNKER'></div>";
	echo "<div class='navBar'>";
	echo "<a href='/'>home</a>";
	echo "<a href='/siteinfo.php?i=about'>about</a>";
	echo "<a href='/index.php?b=paranormal'>paranormal</a>";
	echo "<a href='/index.php?b=tech'>tech</a>";
	echo "<a href='/index.php?b=science'>science</a>";
	echo "<a href='/index.php?b=general'>general</a>";
	echo "<a href='/index.php?b=politics'>politics</a>";
	echo "<a href='/index.php?b=news'>news</a>";
	echo "<a href='/index.php?b=shtf'>shtf</a>";
	echo "<a href='/index.php?b=movies'>movies</a>";
	echo "<a href='/index.php?b=music'>music</a>";
	echo "<a href='/siteinfo.php?i=chat'>chat</a>";
	echo "<a href='/siteinfo.php?i=rss'>rss</a>";
	echo "<div class='leftNavItems'>";
	echo "<a id='theme' onclick='changeTheme()'>💡</a>";
	echo "</div>";
	echo "</div>";
	// emoji bloat
	$emoji = " ";
	switch ($_dbName) {
		case 'news':
			$emoji = "📰";
			break;
		case 'tech':
			$emoji = "💻";
			break;
		case 'general':
			$emoji = "🗣️";
			break;
		case 'paranormal':
			$emoji = "👽";
			break;
		case 'shtf':
			$emoji = "💥";
			break;
		case 'science':
			$emoji = "🔬";
			break;
		case 'politics':
			$emoji = "🔥";
			break;
		case 'movies':
			$emoji = "🎬";
			break;
		case 'music':
			$emoji = "🎸";
			break;
		default:
			$emoji = " ";
			break;
	}
	echo "<div id='warning'>";
	echo "Phones are not computers and you are making the internet worse by trying to use them as such.";
	echo "</div>";
	$current_date = date('m-d-Y H:i:s');
	$timezone = date_default_timezone_get();
	echo "<div class='indexAdSpace'></div>";
	echo "<div id='serverTime'><div id='currentDate'>Server time: ".$current_date." ".$timezone."</div></div><br>";
	echo "<div class='catagory'>".$emoji." /$_dbName/";
	echo "<div class='newThreadButton'><a onclick='moveReplyInIndex()'>[new thread]</a></div>";
	echo "</div>";
	echo "<div id='top'></div>";
	echo "<div class='indexList'>";
}

function printMainIndex() {
	$serverName = "localhost";
	$userName = "textboarduser";
	$password = "ndsfu734Y%#$";

	echo "<!DOCTYPE html>";
	echo "<html lang='en'>";
	echo "<head><title>";
	echo "Paranormal, Conspiracies, Survivalism - Textbunker.net";
	echo "</title><meta charset='UTF-8'>";
	echo "<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />";
	echo "<meta name='description' content='A textboard with an emphasis on survivalism and the paranormal.'>";
	echo "<meta name='keywords' content='survivalist,survival,forum,shtf,paranormal,ufos,aliens,uap,textboard'>";
	echo "<link id='themeLink' rel='stylesheet' type='text/css' href='forest.css'></head>";
	echo "<script src='/site.js'></script>";
	echo "<body onload='loadTheme()'>";
	echo "<div id='cnt'>";
	echo "<div id='logoCnt'><img class='logoBanner' src='logo.png' alt='text BUNKER'></div>";
	echo "<div class='navBar'>";
	echo "<a href='/'>home</a>";
	echo "<a href='/siteinfo.php?i=about'>about</a>";
	echo "<a href='/index.php?b=paranormal'>paranormal</a>";
	echo "<a href='/index.php?b=tech'>tech</a>";
	echo "<a href='/index.php?b=science'>science</a>";
	echo "<a href='/index.php?b=general'>general</a>";
	echo "<a href='/index.php?b=politics'>politics</a>";
	echo "<a href='/index.php?b=news'>news</a>";
	echo "<a href='/index.php?b=shtf'>shtf</a>";
	echo "<a href='/index.php?b=movies'>movies</a>";
	echo "<a href='/index.php?b=music'>music</a>";
	echo "<a href='/siteinfo.php?i=chat'>chat</a>";
	echo "<a href='/siteinfo.php?i=rss'>rss</a>";
	echo "<div class='leftNavItems'>";
	echo "<a id='theme' onclick='changeTheme()'>💡</a>";
	echo "</div>";
	echo "</div>";
	echo "<div id='warning'>";
	echo "Phones are not computers and you are making the internet worse by trying to use them as such.";
	echo "</div>";
	echo "<div class='indexAdSpace'></div>";
	$current_date = date('m-d-Y H:i:s');
	$timezone = date_default_timezone_get();
	echo "<div id='serverTime'><div id='currentDate'>Server time: ".$current_date." ".$timezone."</div></div><br>";
	echo "<div class='indexList'>";
	

	// Create array of databases
	$db_array = ['news', 'shtf', 'paranormal', 'general', 'tech', 'science', 'politics', 'movies', 'music'];
	foreach ($db_array as $val) {
		// bloat to stick an emoji in front of the catagory
		switch ($val) {
			case "news":
				echo "<div class='catagory'><a class='catagoryLink' href='/index.php?b=news'>📰 /".$val."/</a></div>";
				break;
			case "tech":
				echo "<div class='catagory'><a class='catagoryLink' href='/index.php?b=tech'>💻 /".$val."/</a></div>";
				break;
			case "general":
				echo "<div class='catagory'><a class='catagoryLink' href='/index.php?b=general'>🗣️ /".$val."/</a></div>";
				break;
			case "paranormal":
				echo "<div class='catagory'><a class='catagoryLink' href='/index.php?b=paranormal'>👽 /".$val."/</a></div>";
				break;
			case "shtf":
				echo "<div class='catagory'><a class='catagoryLink' href='/index.php?b=shtf'>💥 /".$val."/</a></div>";
				break;
			case "science":
				echo "<div class='catagory'><a class='catagoryLink' href='/index.php?b=science'>🔬 /".$val."/</a></div>";
				break;
			case "politics":
				echo "<div class='catagory'><a class='catagoryLink' href='/index.php?b=politics'>🔥 /".$val."/</a></div>";
				break;
			case "movies":
				echo "<div class='catagory'><a class='catagoryLink' href='/index.php?b=movies'>🎬 /".$val."/</a></div>";
				break;
			case "music":
				echo "<div class='catagory'><a class='catagoryLink' href='/index.php?b=music'>🎸 /".$val."/</a></div>";
				break;
			default:
				echo "<div class='catagory'>/".$val."/</div>";
				break;
		}
		
		$conn = mysqli_connect($serverName, $userName, $password, $val);
		if ( $conn === false ){
			die("<br>Connection to database failed. ");
		}

		$showTables = "SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema = '$val' ORDER BY UPDATE_TIME DESC";
		if( !$result = mysqli_query($conn, $showTables) ){
			die("Query 1 failed while generating main index.");
		}

		//
		// put 3 table names per db into an array
		//$row[0] = table names
		$i = 0;
		$threadTableArray = array();
		while($row = mysqli_fetch_row($result)){
			if ($i == 4) {
				break;
			}
			$threadTableArray[$i] = $row[0];
			$i++;
		}
		
		// cycle through the array of table names and query each name for threadTitle
		$n = 0;
		while($n != $i){
			$sql = "SELECT MAX(id), MAX(postDate), threadTitle FROM $threadTableArray[$n] WHERE id IN (SELECT id FROM $threadTableArray[$n])";
			mysqli_free_result($result);
			if ( $result = mysqli_query($conn, $sql) ){
				while ( $row = mysqli_fetch_row($result) ){
					$newDateStr = $row[1];
					/*
					// if date is current date print time instead of date
					$dtFromDB = new DateTime($row[1]);
					// Get the current date and time
					$now = new DateTime();
					if ($dtFromDB->format('Y-m-d') == $now->format('Y-m-d')) {
						// If they are the same, print only the time part of the datetime value
						$newDateStr = $dtFromDB->format('H:i:s');
						$newDateStr = $newDateStr . ' utc';
					} else {
						// Copy only first part of timestamp into dateArray
						$dateStr = substr($row[1], 0, 10);
						// Move year to the end of the date
						$year = substr($dateStr, 0, 4);
						$monthDay = substr($dateStr, 5);
						$newDateStr = $monthDay . "-" . $year;
					}*/
					
					// minus 1 from id
					$row[0] = $row[0] - 1;
					// Convert id to str
					$idStr = strval($row[0]);
					
					if (strlen($row[2]) > 0){
						echo "<div class='indexListBox'>";
						echo "<a href='thread.php?b=".$val;
						echo "&t=".$threadTableArray[$n]."'>";
						echo htmlspecialchars_decode($row[2]);
						echo "</a>";
						echo "<div class='indexDate'>";
						echo "Updated: ";
						echo "<div class='indexTime'>".$newDateStr."</div>";
						echo "<div class='pad'></div>";
						echo "Comments: ";
						echo $idStr;
						echo "</div></div>";
					}
			}
				$n++;
			} else {
				mysqli_close($conn);
				die ("<br>SQL Query returned NULL");
			}
		}
		
		mysqli_free_result($result);
		mysqli_close($conn);
	}
	
	echo "</div>";
	echo "<div id='bottom'>Copyright © ".date('Y')." textbunker.net all rights reserved.</div>";
	echo "<div id='webRingContainer'>";
	echo "<p>other cool sites:</p>";
	// output php from webring.php
	include 'webring.php';
	echo "</div>";
	echo "</div></body>";
	echo "<script>doIndexTimes()</script>";
	echo "</html>";
}

function printForm($_dbName) {
	echo "</div>";
	echo "<form id='replyForm' accept-charset='UTF-8'>";
	echo "<div id='captchaWarning'></div>";
	echo "<label for='fname'>Create a new thread:</label><br>";
	
	if ( isset($_POST['data']) ) {
		echo "<textarea class='textContentBox' name='data' maxlength='3000' placeholder='Type your thread here, first new line separates title from thread.' value=".$_POST['data']." required></textarea><br>";
	} else {
		echo "<textarea class='textContentBox' name='data' maxlength='3000' placeholder='Type your thread here, first new line separates title from thread.' required></textarea><br>";
	}
	
	echo "<div class='mediaButtonCnt'>";
	echo "<div class='dropdown'>";
	echo "<button type='button' id='dropbtn'>Embed</button>";
	echo "<div id='dropdown-content'>";
	echo "<a onclick='insertQuoteFromReplyForm()'>Quote</a>";
	echo "<a onclick='insertimg()'>Image</a>";
	echo "<a onclick='insertYouTubeLink()'>YouTube</a>";
	echo "<a onclick='insertVideo()'>Video</a>";
	echo "<a onclick='insertLink()'>Link</a>";
	echo "</div></div></div>";
	echo "<div class='captchaCnt'>";
	echo "<button type='button' id='captchaBtn' onclick='showCaptcha()'>Get Captcha</button>";
	echo "<img class='captchaImage' src='' alt='CAPTCHA' loading='lazy'>";
	echo "<a id='captchaRefreshButton' onclick='refreshCaptcha()'>🔄</a></div>";
	echo "<span>Captcha:</span><br>";
	echo "<input id='captchaInput' type='text' size='15' maxlength='5' name='captcha'>";
	echo "<button type='button' class='submitButton' onclick='submitForm(\"" . $_dbName . "\", null)'>Submit</button>";
	echo "</form>";
	echo "<div id='bottom'>Copyright © ".date('Y')." textbunker.net all rights reserved.</div>";
	echo "<div id='webRingContainer'>";
	echo "<p>other cool sites:</p>";
	// output php from webring.php
	include 'webring.php';
	echo "</div>";
	echo "</div></body>";
	echo "<script>loadFormEmbedControls()</script>";
	echo "<script>doIndexTimes()</script>";
	echo "</html>";
}

// Get URL variables
switch ($_GET['b']) {
	case "news":
		$dbname = "news";
		$title = "News";
		printHeader($dbname, $title);
		break;
	case "paranormal":
		$dbname = "paranormal";
		$title = "Paranormal";
		printHeader($dbname, $title);
		break;
	case "tech":
		$dbname = "tech";
		$title = "Technology";
		printHeader($dbname, $title);
		break;
	case "shtf":
		$dbname = "shtf";
		$title = "Survival and Preparedness";
		printHeader($dbname, $title);
		break;
	case "general":
		$dbname = "general";
		$title = "General off-topic discussions";
		printHeader($dbname, $title);
		break;
	case "science":
		$dbname = "science";
		$title = "Science";
		printHeader($dbname, $title);
		break;
	case "politics":
		$dbname = "politics";
		$title = "Politics";
		printHeader($dbname, $title);
		break;
	case "movies":
		$dbname = "movies";
		$title = "Movies & TV";
		printHeader($dbname, $title);
		break;
	case "music":
		$dbname = "music";
		$title = "Music";
		printHeader($dbname, $title);
		break;
	default:
		printMainIndex();
		die();
}

$conn = mysqli_connect($serverName, $userName, $password, $dbname);
if ( $conn === false ){
	die("<br>Connection to database failed. ");
}

$showTables = "SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema = '$dbname' ORDER BY UPDATE_TIME DESC";
if( !$result = mysqli_query($conn, $showTables) ){
	die("Query 1 failed.");
}

// put all table names into an array
//$row[0] = table names
$i = 0;
$threadTableArray = array();
while($row = mysqli_fetch_row($result)){
	$threadTableArray[$i] = $row[0];
	$i++;
}

// cycle through the array of table names and query each name for threadTitle
$n = 0;
while($n != $i){
	$sql = "SELECT MAX(id), MAX(postDate), threadTitle FROM $threadTableArray[$n] WHERE id IN (SELECT id FROM $threadTableArray[$n])";
	mysqli_free_result($result);
	if ( $result = mysqli_query($conn, $sql) ){
		while ( $row = mysqli_fetch_row($result) ){
			// if date is current date print time instead of date
			/*
			$dtFromDB = new DateTime($row[1]);
			// Get the current date and time
			$now = new DateTime();
			if ($dtFromDB->format('Y-m-d') == $now->format('Y-m-d')) {
				// If they are the same, print only the time part of the datetime value
				$newDateStr = $dtFromDB->format('H:i:s');
				$newDateStr = $newDateStr . ' utc';
			} else {
				// Copy only first part of timestamp into dateArray
				$dateStr = substr($row[1], 0, 10);
				// Move year to the end of the date
				$year = substr($dateStr, 0, 4);
				$monthDay = substr($dateStr, 5);
				$newDateStr = $monthDay . "-" . $year;
			}*/
			$newDateStr = $row[1];
			// minus 1 from id
			$row[0] = $row[0] - 1;
			// Convert id to str
			$idStr = strval($row[0]);
			
			if (strlen($row[2]) > 0){
				echo "<div class='indexListBox'>";
				echo "<a href='thread.php?b=".$dbname;
				echo "&t=".$threadTableArray[$n]."'>";
				echo htmlspecialchars_decode($row[2]);
				echo "</a>";
				echo "<div class='indexDate'>";
				echo "Updated: ";
				echo "<div class='indexTime'>".$newDateStr."</div>";
				echo "<div class='pad'></div>";
				echo "Comments: ";
				echo $idStr;
				echo "</div></div>";
			}
		}
		
		$n++;
	} else {
		mysqli_close($conn);
		//print form in case new board and no threads yet
		printForm($dbname);
		die ("<br>SQL Query returned NULL");
	}
}

//mysqli_free_result($result);
mysqli_close($conn);
printForm($dbname);
?>
