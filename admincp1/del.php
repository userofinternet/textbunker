<?php
$serverName = "localhost";
$userName = "textboarduser";
$password = "ndsfu734Y%#$";
$accessCode = "iliketoeatburgers999@";

echo "<!DOCTYPE html>";

// Check passcode
if (isset($_POST['pass']) && $_POST['pass'] == $accessCode) {
    echo "Please wait... ";
} else {
    die("Password incorrect.");
}

// Handle deleting a reply from a thread
if( isset($_POST['replyToDelete'])
&& !empty($_POST['replyToDelete'])
&& isset($_POST['threadToDelete'])
&& !empty($_POST['threadToDelete'])
&& isset($_POST['board'])
&& !empty($_POST['board']) ) {

    echo "Attempting to delete reply #" .$_POST['replyToDelete']. " from thread id " .$_POST['threadToDelete']. ". ";

    // Open database
	$conn = mysqli_connect($serverName, $userName, $password, $_POST['board']);
	if ( mysqli_connect_errno() ){
		die("Connection to DB failed.");
	}

    // execute SQL
    $delreply = "DELETE FROM `" .$_POST['threadToDelete']. "` WHERE `id` = '" .$_POST['replyToDelete']."'";
    if(mysqli_query($conn, $delreply) != TRUE){
		mysqli_close($conn);
		die("Error deleting reply.");
	}

    mysqli_close($conn);
    echo "Success!";
    echo "<br><a href='index.html'>Click here to return.</a>";
    die();

}


// Handle deleting thread only
if (isset($_POST['threadToDelete'])
&& !empty($_POST['threadToDelete'])
&& isset($_POST['board'])
&& !empty([$_POST['board']])
&& empty($_POST['replyToDelete']) ) {

    echo "Attempting to delete " .$_POST['threadToDelete']. ". ";
    
    // Open database
    $conn = mysqli_connect($serverName, $userName, $password, $_POST['board']);
    if ( mysqli_connect_errno() ){
        die("Connection to DB failed.");
    }
    
    // execute SQL
    $delThread = "DROP TABLE " .$_POST['threadToDelete'];
    if(mysqli_query($conn, $delThread) != TRUE) {
        mysqli_close($conn);
        die("Error deleting thread.");
    }
    
    mysqli_close($conn);
    echo "Success!";
    echo "<br><a href='index.html'>Click here to return.</a>";
    die();

}

echo "Something went wrong. :(";
?>