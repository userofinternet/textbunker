<?php
// Webring websites with name and URL
$webringSites = array(
    array(
        'name' => 'Channel4 BBS',
        'url' => 'https://4-ch.net/4ch.html',
    ),
    array(
        'name' => 'čudan',
        'url' => 'https://textboard.lol',
    ),
    array(
        'name' => 'Erowid',
        'url' => 'https://erowid.org',
    )
    // Add more websites here
);

// Shuffle the webring websites to ignore positions
shuffle($webringSites);

// HTML output
foreach ($webringSites as $site) {
    echo "<a href='" . $site['url'] . "'>" . $site['name'] . "</a></li>";
}

echo "<br><br>Donate and help us keep going.";
echo "<br>Bitcoin: 1HbYm6QfwNbKYYHAU4C5pjLegYRqekuQQZ";
echo "<br>Ethereum: 0xd83bE2f4069927868f66AF91508C23d76609f57A";
?>