<?php
// THIS SOFTWARE IS RELEASED UNDER THE MIT LICENSE.
// CREATED BY TEXTBUNKER.NET CODEBERG.ORG/USEROFINTERNET

// Detect recurring word spam
function spamCheck($postData){
	$MAX_RECURRING_WORDS_ALLOWED = 50;
	$MAX_CHARS_ALLOWED_WITHOUT_SPACES = 50;
	$stringArray = str_word_count($postData, 1);
	$stringArrayLen = count($stringArray);
	if($stringArrayLen === 1){
		$len = mb_strlen($postData, 'UTF-8');
		if($len >= $MAX_CHARS_ALLOWED_WITHOUT_SPACES){
			http_response_code(400);
			exit('This looks like spam. Code 1');
		}
	}
	$lastWordChecked = "00000";
	$recurringWords = 0;
	$firstWordsToCheck = 20;
	for($i=0; $i < $stringArrayLen; $i++){
		if($i <= $firstWordsToCheck && $recurringWords >= 10){
			http_response_code(400);
			exit('This looks like spam. Code 2');
		}
		if( strcmp($stringArray[$i], $lastWordChecked) === 0 ){
			$recurringWords++;
		}
		$lastWordChecked = $stringArray[$i];
	}
	if($stringArrayLen <= 100 && $recurringWords >= $MAX_RECURRING_WORDS_ALLOWED){
		http_response_code(400);
		exit('post has too many recuring words.');
	}

}

// Cross Site Script  & Code Injection Sanitization
function xss_cleaner($input_str) {
    $return_str = str_replace( array('<',';','|','&','>',"'",'"',')','('), array('&lt;','&#58;','&#124;','&#38;','&gt;','&apos;','&#x22;','&#x29;','&#x28;'), $input_str );
    $return_str = str_ireplace( '%3Cscript', '', $return_str );
    return $return_str;
}

function gen_random_string($length=20)
{
    $chars ="abcdefghijklmnopqrstuvwxyz1234567890";//length:36
    $final_rand='';
    for($i=0;$i<$length; $i++)
    {
        $final_rand .= $chars[ rand(0,strlen($chars)-1)];
 
    }
    return $final_rand;
}

function addThreadToRSS($title, $link, $id) {
	// Set the maximum number of items to keep in the RSS feed
	$max_items = 30;

	// Load the existing RSS feed, or create a new one if it doesn't exist
	if (file_exists('/var/www/textbunker.net/rss/feed.xml')) {
		$rss = @simplexml_load_file('/var/www/textbunker.net/rss/feed.xml');
		if (!$rss) {
			// There was an error parsing the RSS feed file
			//exit('Error: The RSS feed file is not valid XML.');
			return;
		}
	} else {
		// The RSS feed file does not exist, so create a new one
		$rss = new SimpleXMLElement('<rss version="2.0"><channel></channel></rss>');
		$rss->channel->addChild('title', $title);
		$rss->channel->addChild('link', $link);
		$rss->channel->addChild('id', $id);
	}

	// Remove the oldest item if the maximum number of items has been reached
	if (count($rss->channel->item) >= $max_items) {
		unset($rss->channel->item[0]);
	}

	// Create a new item
	$item = $rss->channel->addChild('item');
	$item->addChild('title', $title);
	$item->addChild('link', $link);
	$item->addChild('id', $id);

	// Add the item's publication date
	$date = date('D, d M Y H:i:s O');
	$item->addChild('pubDate', $date);

	// Save the modified RSS feed
	if ($rss->asXML('/var/www/textbunker.net/rss/feed.xml') === false) {
		// There was an error saving the RSS feed file
		//exit('Error: Unable to save the RSS feed file.');
		return;
	}


}

// Get board URL variables
switch ($_GET['b']) {
	case "news":
		$dbname = "news";
		break;
	case "paranormal":
		$dbname = "paranormal";
		break;
	case "tech":
		$dbname = "tech";
		break;
	case "shtf":
		$dbname = "shtf";
		break;
	case "general":
		$dbname = "general";
		break;
	case "science":
		$dbname = "science";
		break;
	case "politics":
		$dbname = "politics";
		break;
	case "movies":
		$dbname = "movies";
		break;
	case "music":
		$dbname = "music";
		break;
	default:
        http_response_code(404);
		printDoc("<br>404 Not found.");
		die();
}

$serverName = "localhost";
$userName = "textboarduser";
$password = "ndsfu734Y%#$";

// Thread title and OP maximums and minimums
$MAX_THREAD_SIZE = 3000;
$MIN_THREAD_SIZE = 20;
$MIN_THREAD_TITLE_SIZE = 10;
$MAX_THREAD_TITLE_SIZE = 200;
$MAX_THREADS_ALLOWED = 25;


if( isset($_POST['data']) && !empty($_POST['data']) ){

	session_start();
	// Set the time and count threshold
	$time_threshold = time() - 1800; // 30 minutes ago
	$count_threshold = 2;

	// Get the current request URL and initialize the count
	$current_url = $_SERVER['REQUEST_URI'];
	$count = isset($_SESSION['url_count'][$current_url]) ? $_SESSION['url_count'][$current_url] : 0;

	// Check if the count and time thresholds have been exceeded
	if ($count >= $count_threshold && $_SESSION['url_time'][$current_url] > $time_threshold) {
		http_response_code(400);
		exit('Wait a few minutes before posting again.');
	} else {
		// Increment the count and update the last accessed time
		$_SESSION['url_count'][$current_url] = ++$count;
		$_SESSION['url_time'][$current_url] = time();
	}
	
	// validate captcha response
    if($_POST['captcha'] != $_SESSION['digit']) {
		http_response_code(400);
		exit('The captcha was incorrect.');
	}

    session_destroy();
	
	// Check for recurring word spam
	spamCheck($_POST['data']);
	
	// check post data for spam
	if ( ctype_space($_POST['data']) ) {
		http_response_code(400);
		exit('This looks like spam. Code 1');
	}
	
	// Open database
	$conn = mysqli_connect($serverName, $userName, $password, $dbname);
	if ( mysqli_connect_errno() ){
		http_response_code(400);
		exit("Connection to database failed.");
	}
	
	// clean XSS
	$threadOP = xss_cleaner($_POST['data']);

	// check max thread length
	if(mb_strlen($_POST['data'], 'UTF-8') > $MAX_THREAD_SIZE){
		mysqli_close($conn);
		http_response_code(400);
		exit("Thread discarded, too many characters.");
	}
	
	// check min thread length
	if(mb_strlen($_POST['data'], 'UTF-8') < $MIN_THREAD_SIZE){
		mysqli_close($conn);
		http_response_code(400);
		exit("Thread discarded, not enough characters.");
	}
	
	// create thread title from up to first newline or first 200 chars of threadOP
	// Check if the first character is a newline
	$firstIsNewline = false;
	if (substr($threadOP, 0, 1) === "\n") {
		$firstIsNewline = true;
	}
	$newlinePos = strpos($threadOP, "\n");
	if ($newlinePos !== false && $firstIsNewline === false) {
		$threadTitle = substr($threadOP, 0, $newlinePos);
	} else {
		$threadTitle = substr($threadOP, 0, $MAX_THREAD_TITLE_SIZE);
	}

	if( $threadTitle === false || is_null($threadTitle) ){
	    mysqli_close($conn);
		http_response_code(400);
		exit("Error converting thread title.");
	}

	if ( mb_strlen($threadTitle, 'UTF-8') < $MIN_THREAD_TITLE_SIZE ) {
		mysqli_close($conn);
		http_response_code(400);
		exit("Title must be at least ".$MIN_THREAD_TITLE_SIZE." characters long.");
	}
	
	// Generate a random thread id and create new thread table and columns
	$unique_thread_id = gen_random_string();
	
	$createTable = "CREATE TABLE IF NOT EXISTS $unique_thread_id (id int NOT NULL AUTO_INCREMENT,
	postDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
	threadTitle varchar($MAX_THREAD_TITLE_SIZE) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
	threadOP varchar($MAX_THREAD_SIZE) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
	threadReply varchar($MAX_THREAD_SIZE) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci, PRIMARY KEY (id)) ENGINE=MyISAM";
	
	if(mysqli_query($conn, $createTable) === TRUE){
		//echo "<br>create thread successful.</br>";
	} else {
		mysqli_close($conn);
		http_response_code(400);
		exit('Error creating thread, possibly too many characters.');
	}
	
	// Insert the thread title and thread OP into the new table
	$insertData = "INSERT INTO $unique_thread_id (threadTitle, threadOP) VALUES ('$threadTitle', '$threadOP')";
	if(mysqli_query($conn, $insertData) != TRUE){
		//echo "<br>data insertion successful.";
		//echo "<br>Please wait...";
		//header("refresh:2; url=index.php");
		mysqli_close($conn);
		http_response_code(400);
		exit("Error inserting data.");
	}
	
	// Get list of threads in database and delete the oldest one
	// put all table names into an array
	//$row[0] = table names
	$showTables = "SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema = '$dbname' ORDER BY UPDATE_TIME ASC";
	//mysqli_free_result($result);
	$result = mysqli_query($conn, $showTables);
	if ($result === FALSE){
		http_response_code(400);
		exit("SQL Error 2");
	}
	$i = 0;
	$threadTableArray = array();
	while($row = mysqli_fetch_row($result)){
		$threadTableArray[$i] = $row[0];
		$i++;
	}
	mysqli_free_result($result);
	if($i > $MAX_THREADS_ALLOWED){
		$deleteOldestThread = "DROP TABLE $threadTableArray[0]";
		$result = mysqli_query($conn, $deleteOldestThread);
		mysqli_free_result($result);
		mysqli_close($conn);
	} else {
		mysqli_free_result($result);
		mysqli_close($conn);
	}
	
	$url = "https://textbunker.net/thread.php?b=".$dbname."&t=".$unique_thread_id;
	addThreadToRSS(htmlspecialchars($threadTitle), htmlspecialchars($url), $unique_thread_id);
	
	http_response_code(200);
	echo "Please wait...";
	
	
} else{
	http_response_code(400);
	echo "<br>No data received.";
}
?>
