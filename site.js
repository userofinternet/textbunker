/*
This script is free software.
https://www.gnu.org/licenses/gpl-3.0.txt
Original work by: textbunker.net
*/

//pop up embed menu for form
function loadFormEmbedControls() {
	const dropdownBtn = document.getElementById("dropbtn");
	const dropdownContent = document.getElementById("dropdown-content");

	dropdownBtn.addEventListener("click", function() {
  	dropdownContent.style.display = dropdownContent.style.display === "block" ? "none" : "block";
	});

	dropdownContent.addEventListener("click", function() {
  	dropdownContent.style.display = "none";
	});
}

function doWork() {
    // set default theme and store setting in users browser storage.
    loadTheme();
  
    // Get all elements with class "innerText"
    const elements = document.querySelectorAll('.innerText');

	// Post link urls
	const postLinkregex = /\[postlink\](https?:\/\/textbunker\.net\/[^\]]+)\[\/postlink\]/gi;
  
    // YouTube regex for all YouTube URLs
    const youtubeRegex = /\[youtube\]\s*((?:https?:\/\/)?(?:www\.)?(?:youtube\.com\/watch\?v=|youtu\.be\/)([a-zA-Z0-9_-]{11}))\s*\[\/youtube\]/g;
  
    // Link and quote regex patterns
    const linkRegex = /\[link\]\s*(.*?)\s*\[\/link\]/g;
    // quote regex
    const quoteRegex = /\[quote\]([\s\S]*?)\[\/quote\]/g;
    // Image regex
    //const imageRegex = /\[img\]((?:https?:\/\/)?(?:www\.)?(?:postimg\.cc|i\.postimg\.cc|imgbb\.com)\/.+?)\[\/img\]/g;
    // catbox.moe video hosting
    //const catBoxRegex = /\[catbox\]((?:https?:\/\/)?(?:www\.)?(?:catbox\.moe|files\.catbox\.moe)\/.+?)\[\/catbox\]/g;
	// allow any media urls
    const imageRegex = /\[img\]([\s\S]*?)\[\/img\]/g;
	const videoRegex = /\[video\]([\s\S]*?)\[\/video\]/g;

	// get all elements of time class
	const timeElms = document.querySelectorAll(".time");

	// Loop through all time elements and convert server utc time to users local time
	timeElms.forEach(elms => {
		elms.innerHTML = convertUtcToLocalTime(elms.textContent, 0);
	});
  
    // Loop through each element
    elements.forEach(element => {
      // Get the text content of the element
      let text = escapeHtml(element.textContent);

	  // Replace textbunker post link urls
	  text = text.replace(postLinkregex, '<br><button type="button" class="loadMediaButton" onclick="loadPostFromUrl(this, \'$1\')">Show Post</button><div class="urlText">$1</div>');

  
      // Replace YouTube links within [youtube][/youtube] tags with a load media button
      text = text.replace(youtubeRegex, '<br><button type="button" class="loadMediaButton" onclick="loadYouTubeMedia(this, \'$2\')">YouTube Video</button><div class="urlText">$1</div>');
  
      // Create clickable links
      text = text.replace(linkRegex, '<a href="$1" target="_blank" rel="noopener noreferrer">$1</a>');
  
      // Create a quote
      text = text.replace(quoteRegex, '<div class="quote">$1</div>');
  
      // Insert image
      text = text.replace(imageRegex, '<br><button type="button" class="loadMediaButton" onclick="loadImage(this, \'$1\')">Load Image</button><div class="urlText">$1</div>');
  
      // Insert video from catbox.moe
      text = text.replace(videoRegex, '<br><button type="button" class="loadMediaButton" onclick="loadVideo(this, \'$1\')">Load Video</button><div class="urlText">$1</div>');
  
      // Update the element's HTML with the modified text content
      if (text != null && text != "") {
        element.innerHTML = text;
      }
    });
  }

  function loadPostFromUrl(button, postUrl) {
	const divId = postUrl.split('#').pop(); // Extract the target div ID from the URL
  
	fetch(postUrl)
	  .then(response => response.text())
	  .then(html => {
		const parser = new DOMParser();
		const doc = parser.parseFromString(html, 'text/html');
		const targetDiv = doc.getElementById(divId);
		const previousDiv = targetDiv.previousElementSibling.cloneNode(true);
		previousDiv.classList.remove('comment');
		previousDiv.classList.add('CC');
  
		//console.log('targetDiv:', targetDiv);
		//console.log('previousDiv:', previousDiv);

	const parentElement = button.parentElement;
	parentElement.replaceChild(previousDiv, button);

	// Remove links from CC
	const CC = document.querySelectorAll('.CC');
	CC.forEach(container => {
  		const commentItemsA = container.querySelectorAll('.commentItems a');
  		commentItemsA.forEach(link => {
		link.style.color = 'whitesmoke';
		link.style.textDecoration = 'none';
    	link.onclick = '';
  		});

  		const commentBottomA = container.querySelectorAll('.commentBottom a');
  		commentBottomA.forEach(link => {
    	link.style.display = 'none';
  		});
	});

  });
}
   

  function loadYouTubeMedia(button, videoId) {
	const embedHtml = '<iframe width="480" height="300" src="https://www.youtube.com/embed/' + videoId + '" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br>';
	const videoElement = document.createElement('div');
	videoElement.innerHTML = embedHtml;
	const parentElement = button.parentElement;
	parentElement.replaceChild(videoElement, button);
  }
  
  function loadImage(button, imageUrl) {
	const embedHtml = '<img class="commentImage" src="' + imageUrl + '" loading="lazy"></img>';
	const imageElement = document.createElement('div');
	imageElement.innerHTML = embedHtml;
	const parentElement = button.parentElement;
	parentElement.replaceChild(imageElement, button);
  }
  
  function loadVideo(button, videoUrl) {
	const embedHtml = '<video src="' + videoUrl + '" width="480" height="300" controls>Your browser does not support the video tag.</video>';
	const videoElement = document.createElement('div');
	videoElement.innerHTML = embedHtml;
	const parentElement = button.parentElement;
	parentElement.replaceChild(videoElement, button);
  }
  

function moveReplyInIndex() {
	const rf = document.getElementsByTagName('form');
	rf[0].style.display = 'block';
	const target = document.getElementById('top');
	target.appendChild(rf[0]);
	document.getElementsByTagName('textarea')[0].focus();
}

function moveReply(replyNum) {
	// show the reply form
	const rf = document.getElementsByTagName('form');
	rf[0].style.display = 'block';
	// select the target and element to be moved
	const target = document.getElementById(replyNum);
	const elementToMove = document.getElementById('replyForm');
	
	// move the element by appending it to the target
	target.appendChild(elementToMove);
	
	//insert reply number into Textarea
	var e = document.getElementsByClassName('textContentBox');

	if (replyNum == 0) {
  		if (e[0].value.length === 0) {
    		e[0].value += '@' + 'OP' + '\n';
  		} else {
    		e[0].value += '\n@' + 'OP' + '\n';
  	}
	} else if (replyNum != 9999 && replyNum != 0) {
  		if (e[0].value.length === 0) {
    		e[0].value += '@' + replyNum + '\n';
  		} else {
    		e[0].value += '\n@' + replyNum + '\n';
  		}
	}

	document.getElementsByTagName('textarea')[0].focus();
}

function insertVideo() {
	var t = prompt('Paste video link here');
	if (t != null && t != '') {
		var e = document.getElementsByTagName('textarea');
		e[0].value += '[video]' + t + '[/video]';
	}
}

function insertimg() {
	var t = prompt('Paste image link here');
	if (t != null && t != '') {
		var e = document.getElementsByTagName('textarea');
		e[0].value += '[img]' + t + '[/img]';
	}
}

function insertYouTubeLink() {
	var t = prompt('Paste youtube link here');
	if (t != null && t != '') {
		var e = document.getElementsByTagName('textarea');
		e[0].value += '[youtube]' + t + '[/youtube]';
	}
}

function insertLink() {
	var t = prompt('Paste link here');
	if (t != null && t != '') {
		var e = document.getElementsByTagName('textarea');
		e[0].value += '[link]' + t + '[/link]';
	}
}

function refreshCaptcha() {
	var el = document.getElementsByClassName('captchaImage');
	var imgUrl = "/captcha.php?v=" + Date.now();
	el[0].src = imgUrl;
}

function showCaptcha() {
	// Hide the get captcha button
	document.getElementById("captchaBtn").style.display = "none";
	// Show the captcha refresh button
	document.getElementById('captchaRefreshButton').style.display = "block";
	// Show the image
	var el = document.getElementsByClassName("captchaImage")[0];
	var imgUrl = "/captcha.php?v=" + Date.now();
	el.src = imgUrl;
	el.style.display = "block"
}

function insertQuote(postNum) {
	var e = document.getElementsByClassName('innerText');
	var er = document.getElementsByTagName('textarea');
	var c = e[postNum - 1].textContent;
	er[0].value += '\n' + '[quote]' + c + '[/quote]' + '\n';
	var form = document.getElementById('replyForm');
	var target = document.getElementById(postNum -1);
	target.appendChild(form);
	form.focus();
}

function copyLinkToPost(postNum) {
	const currentLocation = window.location.href;
	const regex = /#bottom/g;
	var url = '[postlink]' + currentLocation.replace(regex, '') + '#' + postNum + '[/postlink]';
	navigator.clipboard.writeText(url);
	alert('Link copied to clipboard');
  }
  

function insertQuoteFromReplyForm() {
	var t = prompt('Paste the text here.');
	if (t != null && t != '') {
		var e = document.getElementsByTagName('textarea');
		e[0].value += '[quote]' + t + '[/quote]';
	}
}

function escapeHtml(text) {
  var map = {
    //'&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };
  
  return text.replace(/[<>"']/g, function(m) { return map[m]; });
  //[&<>"']
}

function loadTheme() {
	var SS = localStorage.getItem('selectedStylesheet');
	if (SS === 'forest.css') {
		setDefaultTheme();
	} else if (SS === 'dark.css') {
		setDarkTheme();
	} else {
		setDefaultTheme();
	}
}


// Functions to change and set themes.
// chatGPT was actually too dumb to do this so i figured it out on my own.
function setDefaultTheme() {
	var stylesheetLink = document.querySelector('link[rel="stylesheet"]');
	stylesheetLink.href = 'forest.css';
	localStorage.setItem('selectedStylesheet', 'forest.css');
	
}

function setDarkTheme() {
	var stylesheetLink = document.querySelector('link[rel="stylesheet"]');
	stylesheetLink.href = 'dark.css';
	localStorage.setItem('selectedStylesheet', 'dark.css');
}

// apply the theme
function changeTheme() {
	var stylesheetLink = document.querySelector('link[rel="stylesheet"]');
	var SS = localStorage.getItem('selectedStylesheet');
	if (SS === 'forest.css') {
		stylesheetLink.href = 'dark.css';
		localStorage.setItem('selectedStylesheet', 'dark.css');
	} else {
		stylesheetLink.href = 'forest.css';
		localStorage.setItem('selectedStylesheet', 'forest.css');
	}
}

// Submit reply form using AJAX
async function submitForm(board, tid) {

	document.getElementById('replyForm').focus();

	document.getElementById('captchaWarning').textContent = 'Please wait...';

	// disable button asap to prevent double posts
	var e = document.querySelectorAll('.submitButton');
	e.disabled = true;

	// check captcha
	var c = document.getElementById('captchaInput');
	if (c.value.length === 0) {
		document.getElementById('captchaWarning').textContent = 'Please enter captcha.';
		e.disabled = false;
		return false;
	}

	var textareaData = document.getElementsByTagName('textarea')[0].value;
	var captchaValue = document.getElementById('captchaInput').value;
  
	try {
		if (tid != null) {
			var url = '/newreply.php?b=' + board + '&t=' + tid;
		} else {
			var url = '/newthread.php?b=' + board;
		}
	  var response = await fetch(url, {
		method: 'POST',
		headers: {
		  'Content-Type': 'application/x-www-form-urlencoded'
		},
		body: 'data=' + encodeURIComponent(textareaData) + '&captcha=' + encodeURIComponent(captchaValue)
	  });

	  var responseData = await response.text();
	  if (responseData === 'Please wait...') {
		// Request completed successfully
		document.getElementById('captchaWarning').textContent = responseData;
		e.disabled = false;
		if (tid != null && tid != 'null') {
			window.location.hash = 'bottom';
		} else {
			window.location.hash = 'serverTime';
		}
		window.location.reload();
	  } else {
		document.getElementById('captchaWarning').textContent = 'Error: ' + responseData;
		throw new Error('Request failed with status ' + response.status);
	  }
	} catch (error) {
	  console.error('Error:', error);
	}

	e.disabled = false;
  }

  // scroll to top or bottom of page
  function scrollToElement(eNUm) {
	// Calculate the offset of the element relative to the document
	var element = document.getElementsByClassName('updown');
	// ugly hack and laziness
	if (eNUm === 0) {
		var OT = document.getElementById('logoCnt').offsetTop;
	} else {
		var OT = element[eNUm].offsetTop;	
	}
  
	// Scroll to the element instantly
	window.scrollTo(0, OT);
  }
  
  function convertUtcToLocalTime(postDate, script) {
	if (script === 1) {
	
		try {
	 		 // Split the string into date and time components
	  	var dateComponents = postDate.split(' ')[0].split('-');
	  	var timeComponents = postDate.split(' ')[1].split(':');
  
	  	// Create a new Date object with the parsed components
	  	var utcDate = new Date();
	  	utcDate.setUTCFullYear(dateComponents[0]);
	  	utcDate.setUTCMonth(dateComponents[1] - 1);
	  	utcDate.setUTCDate(dateComponents[2]);
	  	utcDate.setUTCHours(timeComponents[0]);
	  	utcDate.setUTCMinutes(timeComponents[1]);
	  	utcDate.setUTCSeconds(timeComponents[2]);
  
	  	// Get the user's local date and time
	  	var localDate = new Date();
  
	  	// Compare the date components of the UTC date and local date
	  	var isSameDay =
			utcDate.getUTCFullYear() === localDate.getFullYear() &&
			utcDate.getUTCMonth() === localDate.getMonth() &&
			utcDate.getUTCDate() === localDate.getDate();
  
	  	// Format the output based on whether it's the same day or different day
	  	var formattedDate;
	  	if (isSameDay) {
			formattedDate = utcDate.toLocaleTimeString();
	  	} else {
			formattedDate = utcDate.toLocaleDateString();
	  	}
  
	  	return formattedDate;
		} catch (error) {
	  		console.log(error);
		}
	} else {
		try {
			// Split the string into date and time components
			var dateComponents = postDate.split(' ')[0].split('-');
			var timeComponents = postDate.split(' ')[1].split(':');
		
			// Create a new Date object with the parsed components
			var utcDate = new Date();
			utcDate.setUTCFullYear(dateComponents[0]);
			utcDate.setUTCMonth(dateComponents[1] - 1);
			utcDate.setUTCDate(dateComponents[2]);
			utcDate.setUTCHours(timeComponents[0]);
			utcDate.setUTCMinutes(timeComponents[1]);
			utcDate.setUTCSeconds(timeComponents[2]);
		
			// Convert the UTC date to the user's local time
			var localDate = utcDate.toLocaleString();
		
			return localDate;
		  } catch (error) {
			console.log(error);
		  }
	}
  }

  function displayLocalDate() {
	// Get the current date
	var currentDate = new Date();
  
	// Get the parts of the date
	var year = currentDate.getFullYear();
	var month = currentDate.getMonth() + 1; // Months are zero-based
	var day = currentDate.getDate();
  
	// Create a string representation of the date
	var dateString = month + '/' + day + '/' + year;
  
	// Display the date in a div element
	var divElement = document.getElementById('currentDate');
	divElement.textContent = dateString;
  }

  function doIndexTimes() {
	displayLocalDate();
	
	// get all elements of time class
	const timeElms = document.querySelectorAll(".indexTime");

	// Loop through all time elements and convert server utc time to users local time
	timeElms.forEach(elms => {
		elms.innerHTML = convertUtcToLocalTime(elms.textContent, 1);
	});
  }