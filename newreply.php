<?php
// THIS SOFTWARE IS RELEASED UNDER THE MIT LICENSE.
// CREATED BY TEXTBUNKER.NET CODEBERG.ORG/USEROFINTERNET

// Check for recurring word spam
function spamCheck($postData){
	$MAX_RECURRING_WORDS_ALLOWED = 50;
	$MAX_CHARS_ALLOWED_WITHOUT_SPACES = 50;
	$stringArray = str_word_count($postData, 1);
	$stringArrayLen = count($stringArray);
	if($stringArrayLen === 1){
		$len = mb_strlen($postData, 'UTF-8');
		if($len >= $MAX_CHARS_ALLOWED_WITHOUT_SPACES){
			http_response_code(400);
			exit("Spam detected. Code 1");
		}
	}
	$lastWordChecked = "00000";
	$recurringWords = 0;
	$firstWordsToCheck = 20;
	for($i=0; $i < $stringArrayLen; $i++){
		if($i <= $firstWordsToCheck && $recurringWords >= 10){
			http_response_code(400);
			exit("Spam detected. Code 2");
		}
		if( strcmp($stringArray[$i], $lastWordChecked) === 0 ){
			$recurringWords++;
		}
		$lastWordChecked = $stringArray[$i];
	}
	if($stringArrayLen <= 100 && $recurringWords >= $MAX_RECURRING_WORDS_ALLOWED){
		http_response_code(400);
		exit("Spam detected. code 3");
	}

}

// Cross Site Script  & Code Injection Sanitization
function xss_cleaner($input_str) {
    $return_str = str_replace( array('<',';','|','&','>',"'",'"',')','('), array('&lt;','&#58;','&#124;','&#38;','&gt;','&apos;','&#x22;','&#x29;','&#x28;'), $input_str );
    $return_str = str_ireplace( '%3Cscript', '', $return_str );
    return $return_str;
}

// Get board URL variables
switch ($_GET['b']) {
	case "news":
		$dbname = "news";
		break;
	case "paranormal":
		$dbname = "paranormal";
		break;
	case "tech":
		$dbname = "tech";
		break;
	case "shtf":
		$dbname = "shtf";
		break;
	case "general":
		$dbname = "general";
		break;
	case "science":
		$dbname = "science";
		break;
	case "politics":
		$dbname = "politics";
		break;
	case "movies":
		$dbname = "movies";
		break;
	case "music":
		$dbname = "music";
		break;
	default:
        http_response_code(404);
		printDoc("<br>404 Not found.");
		die();
}

$serverName = "localhost";
$userName = "textboarduser";
$password = "ndsfu734Y%#$";

// max and min reply size in chars
$MAX_REPLY_SIZE = 3000;
$MIN_REPLY_SIZE = 1;
$MAX_REPLIES = 300;
$MAX_NEW_LINES = 100;

if( isset($_POST['data'])
&& isset($_GET['t'])
&& !empty($_GET['t'])
&& !empty($_POST['data']) ){

	session_start();
	// Set the time and count threshold
	$time_threshold = time() - 180; // 3 minutes ago
	$count_threshold = 2;

	// Get the current request URL and initialize the count
	$current_url = $_SERVER['REQUEST_URI'];
	$count = isset($_SESSION['url_count'][$current_url]) ? $_SESSION['url_count'][$current_url] : 0;

	// Check if the count and time thresholds have been exceeded
	if ($count >= $count_threshold && $_SESSION['url_time'][$current_url] > $time_threshold) {
		http_response_code(400);
		exit('Wait a few minutes before posting again.');
	} else {
		// Increment the count and update the last accessed time
		$_SESSION['url_count'][$current_url] = ++$count;
		$_SESSION['url_time'][$current_url] = time();
	}

	// Validate captcha response
    if($_POST['captcha'] != $_SESSION['digit']) {
		http_response_code(400);
		exit("Captcha was incorrect.");
	}

    session_destroy();
	
	// Check for recurring word spam
	spamCheck($_POST['data']);
	
	// check post data for spam
	if ( ctype_space($_POST['data']) ) {
		http_response_code(400);
		exit("Spam detected. code 4");
	}
	
	$conn = mysqli_connect($serverName, $userName, $password, $dbname);
	if ( mysqli_connect_errno() ){
		http_response_code(400);
		exit("Error: No connection to DB.");
	}
	
	// check post data for number of new lines
	$numerOfNewLines = substr_count( $_POST['data'], "\n" );
	if ($numerOfNewLines > $MAX_NEW_LINES) {
		$threadGET = $_GET['t'];
		mysqli_close($conn);
		http_response_code(400);
		exit("Max newlines exceeds".$MAX_NEW_LINES);
	}
	
	// get url paramater and clean it
	$threadID_dirty = xss_cleaner($_GET['t']);

	// get total replies in thread
	$unique_thread_id = mysqli_real_escape_string($conn, $threadID_dirty);
	$sql = "SELECT COUNT(*) FROM $unique_thread_id";
	//mysqli_free_result($result);
	$result = mysqli_query($conn, $sql);
	$row = mysqli_fetch_row($result);
	if($row[0] > $MAX_REPLIES-1){
	    mysqli_close($conn);
		http_response_code(400);
		exit("Thread has reached max replies.");
	}
	
	// check max thread length
	if(mb_strlen($_POST['data'], 'UTF-8') > $MAX_REPLY_SIZE){
		mysqli_close($conn);
		http_response_code(400);
		exit("Post exceeds ".$MAX_REPLY_SIZE." characters.");
	}
	
	// check min thread length
	if(mb_strlen($_POST['data'], 'UTF-8') < $MIN_REPLY_SIZE){
		mysqli_close($conn);
		http_response_code(400);
		exit("Post was too short.");
	}

	// clean XSS
	$threadReply = xss_cleaner($_POST['data']);

	// insert reply into row
	$insertRow = "INSERT INTO $unique_thread_id(threadReply) VALUES ('$threadReply')";
	if(mysqli_query($conn, $insertRow) === TRUE){
		mysqli_free_result($result);
		mysqli_close($conn);
		http_response_code(400);
		exit('Please wait...');
	} else {
		mysqli_free_result($result);
		mysqli_close($conn);
		http_response_code(400);
		exit("Error code 5.");
	}

	//mysqli_close($conn);
	
} else{
	http_response_code(400);
	echo "<br>Error: No data received.";
}
?>
