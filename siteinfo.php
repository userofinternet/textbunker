<?php
// THIS SOFTWARE IS RELEASED UNDER THE MIT LICENSE.
// CREATED BY TEXTBUNKER.NET CODEBERG.ORG/USEROFINTERNET

// Stop php from whining about GET variable not being set.
if ( !isset($_GET['i']) ) {
	$_GET['i'] = "about";
}

function messagePrinter($message) {
	echo "<!DOCTYPE html>";
	echo "<html lang='en'>";
	echo "<head>";
	echo "<title>";
	echo "textBUNKER";
	echo "</title>";
	echo "<meta charset='UTF-8'>";
	echo "<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />";
	echo "<link rel='stylesheet' type='text/css' href='forest.css'>";
	echo "<script src='/site.js'></script>";
	echo "</head>";
	echo "<body onload='loadTheme()'>";
	echo "<div id='cnt'>";
	echo "<div id='logoCnt'><img class='logoBanner' src='logo.png' alt='text BUNKER'></div>";
    echo "<div class='navBar'>";
	echo "<a href='/'>home</a>";
    echo "<a href='/siteinfo.php?i=about'>about</a>";
    echo "<a href='/index.php?b=paranormal'>paranormal</a>";
    echo "<a href='/index.php?b=tech'>tech</a>";
	echo "<a href='/index.php?b=science'>science</a>";
	echo "<a href='/index.php?b=general'>general</a>";
	echo "<a href='/index.php?b=politics'>politics</a>";
    echo "<a href='/index.php?b=news'>news</a>";
    echo "<a href='/index.php?b=shtf'>shtf</a>";
	echo "<a href='/index.php?b=movies'>movies</a>";
	echo "<a href='/index.php?b=music'>music</a>";
    echo "<a href='/siteinfo.php?i=chat'>chat</a>";
	echo "<a href='/siteinfo.php?i=rss'>rss</a>";
	echo "<div class='leftNavItems'>";
	echo "<a id='theme' onclick='changeTheme()'>💡</a>";
	echo "</div>";
    echo "</div>";
	echo "<div id='infoCnt'>";
	echo $message;
	echo "</div>";
	echo "<div id='bottom'>Copyright © ".date('Y')." textbunker.net all rights reserved.</div>";
	echo "</div>";
	echo "<body></html>";
}

// beginning of script

$limitMsg = "Woah!, Slow it down a bit there..<br><br>
You're doing that too much. Wait a few minutes before trying again.<br><br>
Also please note that the site has strict rate limiting enabled so if your trying to post a new thread you
can only do that once per 30 minutes and reply only once per 3 minutes. this includes threads or replies that fail to post.
you can only attempt to post within those time frames. i know it kinda sucks but that's the best way to fight ddos and spam.
<br><br><button type='button' onclick='history.back()'>Click to go back</button>";

$aboutMsg = "<h3>About</h3><br>
Textbunker.net is a textboard with an emphasis on survival, paranormal, and technology topics.<br>
This site is pretty cool and actually respects your privacy unlike a lot of other boards,<br>
We only use minimal javascript, no tracking.<br><br>
The goal of this site is to create a place where people can discuss paranormal topics
without having to worry about violent or pornographic images appearing on their screen.<br>
Afterall the internet should be a place where people come to share information with others,
not a place where people go to get traumatized.<br><br>
Linking to video or image files is OK as long as the content isn't violent or pornographic in nature.<br><br>
Threads get deleted automatically based on how popular they are and they are NOT archived,
so if you want to preserve the information in a thread you may want to periodically take a screenshot of it
or use a script to make a copy.<br><br>
We discourage the use of phones to view this website, Posting from phones is bad for the internet,
it creates an environment where every site becomes twitter, where the quantity of posts increases but the quality of the posts decreases.
So try to post mostly from a suitable desktop or laptop computer. Do it for yourself but also for the quality and health of the internet.<br><br>
All trademarks and copyrights posted to this website are owned by their respective parties. any data uploaded to this site are the responsibility of the Poster. Comments are owned by the Poster.<br><br>
You must be at least 18 years old to use this website.<br><br>
We ask that you keep discussion here civil and on topic.<br><br>
<h3>FAQ</h3>
<br>
Q: How do i embed a post from another thread?<br>
A: click on the post number and it will copy to your clipboard, then just paste into the form.<br><br>
Q: How do i embed images and video?<br>
A: Use the embed menu from the posting form.<br><br>
Q: Where do i find media to embed?<br>
A: Anywhere you can find it, you can embed media from any site that allow it.<br><br>
Q: How do i report a post that violated the TOS?<br>
A: Use the contact form below.<br>
<br>
Q: How can i advertise on textbunker.net?<br>
A: Use the contact form below.<br>
<br>
Q: My post failed, why?<br>
A: The board has various anti-spam features enabled, Try reformatting and try again.<br>
<br>
<a href='https://forms.gle/ZaTouwnab2ZUPZFq6'>Contact Us.</a>
<br>
Thank you.";

$chatMsg = "<br><iframe src='https://qchat.rizon.net/?channels=conspiracy%2Cdeadinternettheory&uio=MTE9MTMzJjE1PXRydWUc3' width='100%' height='600'></iframe>";

$rssMsg = "<h3>RSS Feeds</h3><br>User submited threads: https://textbunker.net/rss/feed.xml<br>News bot posts: https://textbunker.net/rss/newsbot.xml";

// Get URL variables
switch ($_GET['i']) {
	case "limit":
		messagePrinter($limitMsg);
		break;
	case "chat":
		messagePrinter($chatMsg);
		break;
	case "about":
		messagePrinter($aboutMsg);
		break;
	case "rss":
		messagePrinter($rssMsg);
		break;
}


?>